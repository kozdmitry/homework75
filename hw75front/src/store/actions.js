import axios from "axios";

export const POST_ENCODED = 'POST_ENCODED';
export const REQUEST = 'REQUEST';

export const CHANGE_INPUT = 'CHANGE_INPUT';
export const ADD_DECODED = 'ADD_DECODED';
export const ADD_ENCODED = 'ADD_ENCODED';
export const ERROR = 'ERROR';

export const changeInput = (event) => ({type: CHANGE_INPUT, event})
export const addDecoded = (data) => ({type: ADD_DECODED, data});
export const addEncoded = data => ({type: ADD_ENCODED, data})

export const request = () => ({type: REQUEST});
export const error = error => ({type: ERROR, error});


export const postDecodeMessage = (input) => {
    return async dispatch => {
        dispatch(request());
        try{
            const obj = {
                message: input.decoded,
                password: input.password,
            };
            const response = await axios.post('http://localhost:8000/decode', obj);
            dispatch(addEncoded(response.data.decoded))
        } catch (e) {
            dispatch(error(e));
        }
    }
};


export const postEncoded= (input) => {
    return async dispatch => {
        try {
            const obj = {
                message: input.encoded,
                password: input.password,
            };
            const response = await axios.post ('http://localhost:8000/encode', obj);
            dispatch(addDecoded(response.data.encoded))
        } catch (e) {
            console.log(e)
        }
    }
};