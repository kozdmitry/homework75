import {
    ADD_DECODED, ADD_ENCODED,
    CHANGE_INPUT,
    ERROR, REQUEST,
    POST_ENCODED
} from "./actions";

const initialState = {
    decoded: "",
    encoded: "",
    password: '',
    loading: false,
    error: false,
};

const reducer = (state = initialState, action) => {
    switch (action.type){
        case POST_ENCODED:
            return {...state, encoded: action.input};
        case REQUEST:
            return {...state, loading: !state.loading}
        case ERROR:
            return {...state, error: action.error}
        case CHANGE_INPUT:
            return {...state, [action.event.name]: action.event.value};
        case ADD_DECODED:
            return {...state, decoded: action.data}
        case ADD_ENCODED:
            return {...state, encoded: action.data}
        default:
            return state;
    }

};

export default reducer;
